package com.bughub.lendingplatform.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.bughub.lendingplatform.entity.Borrower;
import com.bughub.lendingplatform.mapper.BorrowerMapper;
import com.bughub.lendingplatform.service.BorrowerService;
import com.bughub.lendingplatform.entity.User;
import com.bughub.lendingplatform.mapper.UserMapper;
import com.bughub.lendingplatform.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import java.util.List;


@Service
public class BorrowerServiceImpl implements BorrowerService {

    @Qualifier("BorrowerMapper")
    @Autowired
    BorrowerMapper borrowerMapper;

    @Qualifier("userMapper")
    @Autowired
    UserMapper userMapper;



    @Override
    public Object getBorrowerInfoByManager(String userName){
        String returnJson;
        Borrower borrower;
        String status;
        Boolean isNull;
        borrower = borrowerMapper.selectByPrimaryKey(userName);


        User user;
        user = userMapper.selectByPrimaryKey(userName);

        isNull = (borrower == null);
        if(!isNull){
            status = "200";

            JSONObject res = new JSONObject();
            res.put("status", status);
            res.put("userName",borrower.getUserName());
            res.put("userCode",user.getUserCode());
            res.put("borrowerName",borrower.getBorrowerName());
            res.put("borrowerTelephone",borrower.getBorrowerTelephone());
            res.put("borrowerAddress",borrower.getBorrowerAddress());
            res.put("borrowerEmail",borrower.getBorrowerEmail());
            res.put("borrowerLink",borrower.getBorrowerLink());

            return res;

        }
        else{
            status = "404";

            JSONObject res = new JSONObject();
            res.put("status", status);

            return res;
        }
    }


    @Override
    public Object updateRankLevelByManager(String userName, Boolean isValid){
        String returnJson;
        String status;
        Borrower borrower;
        Boolean isNull;
        borrower = borrowerMapper.selectByPrimaryKey(userName);
        isNull = (borrower == null);
        String borrowerRankLevel;
        JSONObject res = new JSONObject();
        if(!isNull){
            if(isValid){
                borrowerRankLevel = borrower.getBorrowerRankLevel();
                if(borrowerRankLevel.equals("2")){
                    status = "404";
                    res.put("status", status);
                    return res;
                }
                else{
                    borrowerRankLevel = "2";
                    borrowerMapper.updateRankLevelByPrimaryKey(userName, borrowerRankLevel);
                    status = "200";
                    res.put("status", status);
                    return res;
                }
            }
            else{
                borrowerMapper.deleteByPrimaryKey(userName);
                status = "200";
                res.put("status", status);
                return res;
            }
        }
        else{
            status = "404";
            res.put("status", status);
            return res;
        }
    }

    @Override
    public JSONArray getRankLevelOneBorrowerByManager(){
        String returnJson;
        Boolean isNull;
        String status;
        List<Borrower> borrowerList;
        String borrowerRankLevel;
        borrowerRankLevel = "1";
        borrowerList = borrowerMapper.selectByRankLevel(borrowerRankLevel);
        int size = borrowerList.size();
        isNull = (size == 0);
        Borrower borrower;
        if(!isNull){
            status = "200";
            JSONArray res = new JSONArray();

            JSONObject temp = new JSONObject();
            temp.put("status", status);
            res.add(temp);
            for(int j=0;j<borrowerList.size();j++)
            {

                borrower = borrowerList.get(j);
                JSONObject info = new JSONObject();
                info.put("userName", borrower.getUserName());
                res.add(info);
            }

            return res;
        }
        else{
            status = "404";
            JSONArray res = new JSONArray();

            JSONObject temp = new JSONObject();
            temp.put("status", status);
            res.add(temp);
            return res;
        }

    }

}
