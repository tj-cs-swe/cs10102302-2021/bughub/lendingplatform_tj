package com.bughub.lendingplatform.entity;

public class Lender {
    private String userName;

    private String lenderName;

    private String lenderTelephone;

    private String lenderAddress;

    private String lenderEmail;

    private String lenderRankLevel;

    private String lenderLink;

    public Lender(String userName, String lenderName, String lenderTelephone, String lenderAddress, String lenderEmail, String lenderRankLevel, String lenderLink) {
        this.userName = userName;
        this.lenderName = lenderName;
        this.lenderTelephone = lenderTelephone;
        this.lenderAddress = lenderAddress;
        this.lenderEmail = lenderEmail;
        this.lenderRankLevel = lenderRankLevel;
        this.lenderLink = lenderLink;
    }

    public Lender() {
        super();
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getLenderName() {
        return lenderName;
    }

    public void setLenderName(String lenderName) {
        this.lenderName = lenderName;
    }

    public String getLenderTelephone() {
        return lenderTelephone;
    }

    public void setLenderTelephone(String lenderTelephone) {
        this.lenderTelephone = lenderTelephone;
    }

    public String getLenderAddress() {
        return lenderAddress;
    }

    public void setLenderAddress(String lenderAddress) {
        this.lenderAddress = lenderAddress;
    }

    public String getLenderEmail() {
        return lenderEmail;
    }

    public void setLenderEmail(String lenderEmail) {
        this.lenderEmail = lenderEmail;
    }

    public String getLenderRankLevel() {
        return lenderRankLevel;
    }

    public void setLenderRankLevel(String lenderRankLevel) {
        this.lenderRankLevel = lenderRankLevel;
    }

    public String getLenderLink() {
        return lenderLink;
    }

    public void setLenderLink(String lenderLink) {
        this.lenderLink = lenderLink;
    }
}