package com.bughub.lendingplatform.mapper;

import com.bughub.lendingplatform.entity.FinancialTrade;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

@Mapper
@Component("FinancialTradeMapper")
public interface FinancialTradeMapper {
    int deleteByPrimaryKey(Integer tradeInfoId);

    int insert(FinancialTrade record);

    int insertSelective(FinancialTrade record);

    FinancialTrade selectByPrimaryKey(Integer tradeInfoId);

    List<FinancialTrade> selectByLenderName(String lenderName);

    int updateByPrimaryKeySelective(FinancialTrade record);

    int updateByPrimaryKey(FinancialTrade record);

    int updateIsValidByPrimaryKey(Integer tradeInfoId, Integer isValid);

    int updateDateValidTime(Integer tradeInfoId, Date tradeValidTime);

    List<FinancialTrade> selectByBorrowerName(String userName);


}