 import { get, post } from './http'

 // http://119.45.246.113/api/showLoans

/**
 * getLogin方法，对应发送的登录请求
 * @param {Object} params [请求时携带的参数]
 */
function getLogin (params) {
  return get('/api/login', params)
}

/**
 * getLogin方法，对应发送的登录请求
 * @param {Object} params [请求时携带的参数]
 * params:
 * userName
 * password
 *
 * 返回:
 * status:
 * userType:
 */
function getRegister (params) {
  return get('/api/register', params)
}

/*
*@params userName
        userType
*
返回:
userName,
userType
userCode
*/
function getshowRegister (params) {
  return get('/api/showRegisters', params)
}

/*
*@params userName
        isValid
*
返回:
status
*/
function auditRegister (params) {
  return get('/api/auditRegisters', params)
}

/*

userName:企业用户名（如BaiDu）
userCode:企业编码（工商注册号如310115400176517）
borrower_name:企业名（如百度（中国）有限公司）
borrower_telephone:企业电话
borrower_address:企业地址
borrower_email:企业邮箱
borrower_link:企业天眼查链接

返回 status
*/
// http://119.45.246.113/api/showProducts
function manageEnterpriseInfo (params) {
  return get('/api/manageEnterpriseInfo', params)
}

/*
/api/showEnterprises
请求参数:无
返回参数:status:返回状态
       :userName:用户名(企业)
*/
function showEnterprises (params) {
  return get('/api/showEnterprises', params)
}

// userName:(企业)用户名
// 返回类型
//    "status": "返回状态（需要规定状态数以确认是否有企业信息）",
// "userName": "企业用户名（如BaiDu）",
// "userCode": "企业编码（工商注册号如310115400176517）",
// "borrowerName": "企业名（如百度（中国）有限公司）",
// "borrowerTelephone": "企业电话",
// "borrowerAddress": "企业地址",
// "borrowerEmail": "企业邮箱",
// "borrowerLink": "企业天眼查链接"
// }
function showEnterpriseInfo (params) {
  return get('/api/showEnterprisesInfo', params)
}

// userName:（企业）用户名
// isValid:是否通过申请
// {
//   "status": "返回状态"
// }
function auditEnterprises (params) {
  return get('/api/auditEnterprises', params)
}

/*
borrowerName:买方债方（企业）
lenderName:卖方贷方（机构）
productId:产品id
amount:交易金额
bankCard:银行卡号（支付方式）
返回参数:status
*/
function purchaseProduct (params) {
  return get('/api/purchaseProduct', params)
}

// [{
//   "status": "返回状态",
//   "productId": "理财产品id",
//   "productProvider": "对应金融机构名称",
//   "productCode": "理财产品编码",
//   "productName": "理财产品名称",
//   "productCurrency": "人民币",
//   "productAsset": "资产规模",
//   "productNetValue": "净值",
//   "productRiseOneMonth": "近 1 月涨幅，所有涨幅数据形成一个图表",
//   "productRiseTwoMonth": "近2-近1月涨幅",
//   "productRiseThreeMonth": "理解同上",
//   "productRiseFourMonth": "理解同上",
//   "productRiseFiveMonth": "理解同上",
//   "productRiseSixMonth": "理解同上",
//   "productFundManager": "基金经理",
//   "productCreateTime": "成立时间"
// }]
function showProducts (params) {
  return get('/api/showProducts', params)
}

function showMyProducts (params) {
  return get('/api/showMyProducts', params)
}

// userName:（机构）用户名
// [{
//   "status": "返回状态",
//   "tradeInfoId": "用来区分交易记录的 id 号",
//   "productId": "产品id",
//   "amount": "交易金额",
//   "bankCard": "银行卡号（支付方式）",
//   "tradeCreateTime": "交易创建时间"
// }]
function showProductPurchase (params) {
  return get('/api/showProductPurchase', params)
}

// tradeInfoId:用来区分交易记录的 id 号
// isValid:是否同意交易
// {
//   "status": "返回状态"
// }
function confirmProductPurchase (params) {
  return post('/api/confirmProductPurchase', params)
}

// [{
//   "status": "",
//   "loanId": "贷款产品id",
//   "loanProvider": "对应金融机构名称",
//   "loanCode": "贷款产品编码",
//   "loanName": "贷款产品名称（贷款产品名称可以取“自强计划”之类的）",
//   "loanCurrency": "默认人民币",
//   "loanAsset": "贷款最多金额（为简便起见值对应（0，loan_asset）区间",
//   "loanAnnualInterest": "年息（5.1实际上表示的是5.1%）",
//   "loanAdditionalInfo": "附加信息（“此项贷款针对…的微企）"
// }]
function showLoans (params) {
  return get('/api/showLoans', params)
}

function showMyLoans (params) {
  return get('/api/showMyLoans', params)
}

// borrowerName:买房债方（企业）
// lenderName:卖方债方（机构）
// loanId:贷款产品id
// amount:贷款金额
// bankCard:银行卡号（收款方式）
// {
//   "status": "成功200，失败404之类"
// }
function getpurchaseLoan (params) {
  return get('/api/purchaseLoan', params)
}

// userName:（机构）用户名
// userType:用户类型（机构）
// [{
//   "status": "",
//   "trade_info_id": "用来区分交易记录的id号",
//   "loan_id": "产品id",
//   "amount": "贷款金额",
//   "bank_card": "银行卡号（支付方式）",
//   "trade_create_time": "交易创建时间"
// }]
function showLoanPurchase (params) {
  return get('/api/showLoanPurchase', params)
}

function getMessageSend (params) {
  return get('/api/getMessageSent', params)
}

function getMessageReceived (params) {
  return get('/api/getMessageReceived', params)
}

function showSingleProduct (params) {
  return get('/api/showSingleProduct', params)
}

function showSingleLoan (params) {
  return get('/api/showSingleLoan', params)
}

function getSendMessage (params) {
  return get('/api/sendMessage', params)
}

function publishProduct (params) {
  return get('/api/publishProduct', params)
}

function publishLoan (params) {
  return get('/api/publishLoan', params)
}

export {
  getLogin,
  getRegister,
  auditRegister,
  manageEnterpriseInfo,
  showEnterprises,
  showEnterpriseInfo,
  auditEnterprises,
  showProducts,
  purchaseProduct,
  showProductPurchase,
  confirmProductPurchase,
  showLoans,
  showMyLoans,
  showMyProducts,
  getpurchaseLoan,
  showLoanPurchase,
  getMessageSend,
  getMessageReceived,
  showSingleProduct,
  showSingleLoan,
  getSendMessage,
  getshowRegister,
  publishProduct,
  publishLoan,
}
