import Vue from 'vue'
import App from './App.vue'
import router from './router'
import vuetify from './plugins/vuetify' // 注册核心插件vuetify
import './plugins' // ES6语法。等价于找index.js
import store from './store' // 应该是使用了vuex下面的东西
import { sync } from 'vuex-router-sync'//

Vue.config.productionTip = false // 阻止vue启动产生的生产消息

sync(store, router) // store has a module route.

new Vue({
  router, // 注册router,store,vuetify
  vuetify,
  store,
  render: h => h(App),
}).$mount('#app')

// sync：adds a route module into the store, containing the state representing the current route.
