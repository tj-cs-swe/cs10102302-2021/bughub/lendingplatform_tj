package com.bughub.lendingplatform.service;

import com.bughub.lendingplatform.entity.Message;

import java.util.List;

public interface MessageService {
    List<Message> getMessageSent(String messageSender);
    List<Message> getMessageReceived(String messageReceiver);
    boolean sendMessage(String messageSender, String messageReceiver, String messageTitle, String messageText);
}
