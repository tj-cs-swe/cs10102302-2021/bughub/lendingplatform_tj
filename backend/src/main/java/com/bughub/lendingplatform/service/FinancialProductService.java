package com.bughub.lendingplatform.service;

import com.alibaba.fastjson.JSONArray;
import com.bughub.lendingplatform.entity.FinancialProduct;
import jdk.security.jarsigner.JarSigner;

import java.util.List;

public interface FinancialProductService {
    List<FinancialProduct> getAllProduct();

    Boolean addProduct(String userName, String productCode, String productName, String productCurrency, Float productAsset, Float productNetValue, String productFundManager);

    FinancialProduct getSingleProduct(Integer productId);

    JSONArray getMyProduct(String userName);
}
