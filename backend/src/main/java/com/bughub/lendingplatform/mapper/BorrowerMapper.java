package com.bughub.lendingplatform.mapper;

import com.bughub.lendingplatform.entity.Borrower;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

import java.util.List;

@Mapper
@Component("BorrowerMapper")
public interface BorrowerMapper {
    int deleteByPrimaryKey(String userName);

    int insert(Borrower record);

    int insertSelective(Borrower record);

    Borrower selectByPrimaryKey(String userName);

    int updateByPrimaryKeySelective(Borrower record);

    int updateByPrimaryKeyWithBLOBs(Borrower record);

    int updateByPrimaryKey(Borrower record);

    List<Borrower> selectByRankLevel(String borrowerRankLevel);

    int updateRankLevelByPrimaryKey(String userName, String borrowerRankLevel);
}