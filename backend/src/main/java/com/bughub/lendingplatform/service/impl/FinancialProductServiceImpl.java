package com.bughub.lendingplatform.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.bughub.lendingplatform.entity.Borrower;
import com.bughub.lendingplatform.entity.FinancialProduct;
import com.bughub.lendingplatform.entity.FinancialTrade;
import com.bughub.lendingplatform.entity.Lender;
import com.bughub.lendingplatform.mapper.BorrowerMapper;
import com.bughub.lendingplatform.mapper.FinancialProductMapper;
import com.bughub.lendingplatform.mapper.FinancialTradeMapper;
import com.bughub.lendingplatform.mapper.LenderMapper;
import com.bughub.lendingplatform.service.FinancialProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class FinancialProductServiceImpl implements FinancialProductService {

    @Qualifier("FinancialProductMapper")
    @Autowired
    FinancialProductMapper financialProductMapper;

    @Override
    public List<FinancialProduct> getAllProduct(){
        return financialProductMapper.selectAllProduct();
    }

    @Override
    public Boolean addProduct(String userName, String productCode, String productName, String productCurrency, Float productAsset, Float productNetValue, String productFundManager){
        if (!productCurrency.equals("人民币"))
            return false;

        if(financialProductMapper.selectByCodeAndName(productCode,productName) == null){
            FinancialProduct financialProduct = new FinancialProduct();
            financialProduct.setProductAsset(productAsset);
            financialProduct.setProductCode(productCode);
            financialProduct.setProductProvider(userName);
            financialProduct.setProductCurrency(productCurrency);
            financialProduct.setProductFundManager(productFundManager);
            financialProduct.setProductNetValue(productNetValue);
            financialProduct.setProductName(productName);
            financialProduct.setProductCreateTime(new Date());

            financialProductMapper.insert(financialProduct);

            return true;

        }
        else{
            return false;
        }
    }

    @Override
    public FinancialProduct getSingleProduct(Integer productId){

        FinancialProduct financialProduct;
        financialProduct = financialProductMapper.selectByPrimaryKey(productId);

        return financialProduct;

    }

    @Qualifier("BorrowerMapper")
    @Autowired
    BorrowerMapper borrowerMapper;

    @Qualifier("FinancialTradeMapper")
    @Autowired
    FinancialTradeMapper financialTradeMapper;

    @Qualifier("LenderMapper")
    @Autowired
    LenderMapper lenderMapper;


    @Override
    public JSONArray getMyProduct(String userName){
        Borrower borrower = borrowerMapper.selectByPrimaryKey(userName);
        Lender lender = lenderMapper.selectByPrimaryKey(userName);
        //FinancialProduct financialProduct;
        List<FinancialTrade> financialTradeList;
        //List<FinancialProduct> financialProductList;
        JSONArray res = new JSONArray();
        FinancialProduct financialProduct;
        if(borrower != null)
        {
            Integer productId;
            financialTradeList = financialTradeMapper.selectByBorrowerName(userName);
            for(int i=0;i<financialTradeList.size();i++){
                productId = financialTradeList.get(i).getProductId();
                financialProduct = financialProductMapper.selectByPrimaryKey(productId);
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("productId", financialProduct.getProductId());
                jsonObject.put("productProvider", financialProduct.getProductProvider());
                jsonObject.put("productCode", financialProduct.getProductCode());
                jsonObject.put("productName", financialProduct.getProductName());
                jsonObject.put("productCurrency", financialProduct.getProductCurrency());
                jsonObject.put("productAsset", financialProduct.getProductAsset());
                jsonObject.put("productNetValue", financialProduct.getProductNetValue());
                jsonObject.put("productRiseOneMonth", financialProduct.getProductRiseOneMonth());
                jsonObject.put("productRiseTwoMonth", financialProduct.getProductRiseTwoMonth());
                jsonObject.put("productRiseThreeMonth", financialProduct.getProductRiseThreeMonth());
                jsonObject.put("productRiseFourMonth", financialProduct.getProductRiseFourMonth());
                jsonObject.put("productRiseFiveMonth", financialProduct.getProductRiseFiveMonth());
                jsonObject.put("productRiseSixMonth", financialProduct.getProductRiseSixMonth());
                jsonObject.put("productFundManager", financialProduct.getProductFundManager());
                jsonObject.put("productCreateTime", financialProduct.getProductCreateTime());
                jsonObject.put("isValid", financialTradeList.get(i).getIsValid());
                res.add(jsonObject);

            }
            if(financialTradeList.size() > 0){
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("status", "200");

                res.add(jsonObject);
            }
            else{
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("status", "404");

                res.add(jsonObject);
            }
            return res;
        }
        else if(lender != null){
            List<FinancialProduct> financialProductList = financialProductMapper.selectByProductProvider(userName);
            if(financialProductList.size() == 0){
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("status", "404");

                res.add(jsonObject);

                return res;
            }
            else{
                for(int i=0;i<financialProductList.size();i++){
                    JSONObject jsonObject = new JSONObject();
                    financialProduct = financialProductList.get(i);
                    jsonObject.put("productId", financialProduct.getProductId());
                    jsonObject.put("productProvider", financialProduct.getProductProvider());
                    jsonObject.put("productCode", financialProduct.getProductCode());
                    jsonObject.put("productName", financialProduct.getProductName());
                    jsonObject.put("productCurrency", financialProduct.getProductCurrency());
                    jsonObject.put("productAsset", financialProduct.getProductAsset());
                    jsonObject.put("productNetValue", financialProduct.getProductNetValue());
                    jsonObject.put("productRiseOneMonth", financialProduct.getProductRiseOneMonth());
                    jsonObject.put("productRiseTwoMonth", financialProduct.getProductRiseTwoMonth());
                    jsonObject.put("productRiseThreeMonth", financialProduct.getProductRiseThreeMonth());
                    jsonObject.put("productRiseFourMonth", financialProduct.getProductRiseFourMonth());
                    jsonObject.put("productRiseFiveMonth", financialProduct.getProductRiseFiveMonth());
                    jsonObject.put("productRiseSixMonth", financialProduct.getProductRiseSixMonth());
                    jsonObject.put("productFundManager", financialProduct.getProductFundManager());
                    jsonObject.put("productCreateTime", financialProduct.getProductCreateTime());
                    res.add(jsonObject);
                }
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("status", "200");

                res.add(jsonObject);

                return res;
            }
        }
        else{
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("status", "404");

            res.add(jsonObject);

            return res;
        }
    }

}
