package com.bughub.lendingplatform.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.bughub.lendingplatform.entity.*;
import com.bughub.lendingplatform.mapper.BorrowerMapper;
import com.bughub.lendingplatform.mapper.LenderMapper;
import com.bughub.lendingplatform.mapper.LoanMapper;
import com.bughub.lendingplatform.mapper.LoanTradeMapper;
import com.bughub.lendingplatform.service.LoanTradeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

@Service
public class LoanTradeServiceImpl implements LoanTradeService {

    @Qualifier("LoanTradeMapper")
    @Autowired
    LoanTradeMapper loanTradeMapper;

    @Qualifier("BorrowerMapper")
    @Autowired
    BorrowerMapper borrowerMapper;

    @Qualifier("LenderMapper")
    @Autowired
    LenderMapper lenderMapper;

    @Qualifier("LoanMapper")
    @Autowired
    LoanMapper loanMapper;

   @Override
    public Boolean addNewLoanTrade(String borrowerName, String lenderName, Integer loanId, Float amount, String bankCard){
       Borrower b = borrowerMapper.selectByPrimaryKey(borrowerName);

       if (b == null)
           return false;

       Lender l = lenderMapper.selectByPrimaryKey(lenderName);

       if (l == null)
           return false;

       Loan loan = loanMapper.selectByPrimaryKey(loanId);

       if (loan == null || !loan.getLoanProvider().equals(l.getUserName()))
           return false;

       LoanTrade lt = new LoanTrade();
       lt.setBorrowerName(borrowerName);
       lt.setAmount(amount);
       lt.setTradeCreateTime(new Date());
       lt.setLoanId(loanId);
       lt.setLenderName(lenderName);
       lt.setBankCard(bankCard);
       lt.setIsValid(false);



       loanTradeMapper.insert(lt);

       return true;
    }


    @Override
    public List<LoanTrade> showLoanPurchase(String userName){

       List<LoanTrade> loanTrades;

       loanTrades = loanTradeMapper.selectByLenderName(userName);

        List<LoanTrade> res = new LinkedList<>();

        for (LoanTrade datum : loanTrades) {
            if (!datum.getIsValid())
                res.add(datum);
        }

        return res;


    }


    @Override
    public Object updateIsValid(int tradeInfoId, Boolean isValid){

        JSONObject res = new JSONObject();
        String status;
        if(isValid) {
            LoanTrade loanTrade;
            loanTrade = loanTradeMapper.selectByPrimaryKey(tradeInfoId);
            if(loanTrade == null) {
                status = "404";
                res.put("status",status);
                return res;
            }
            else if(loanTrade.getIsValid()){
                status = "404";
                res.put("status",status);
                return res;
            }
            else{
                loanTradeMapper.updateIsValidByPrimaryKey(tradeInfoId,1);
                Date tradeValidTime = new Date();
                loanTradeMapper.updateDateValidTime(tradeInfoId,tradeValidTime);
                status = "200";
                res.put("status",status);
                return res;
            }

        }
        else{
            status = "404";
            res.put("status",status);
            return res;
        }

    }

}
