
// leadingslash: 如果首字母不是'/',那么我们就加一个'/';

// trailingSlash:如果结束的字母不是'/',那么我们就加一个'/'

// wait：把传入的一个timeout值变成setTimeout这样的一个值。

export function leadingSlash (str) {
  return str.startsWith('/') ? str : '/' + str
}

export function trailingSlash (str) {
  return str.endsWith('/') ? str : str + '/'
}

export const wait = timeout => {
  return new Promise(resolve => setTimeout(resolve, timeout))
}
