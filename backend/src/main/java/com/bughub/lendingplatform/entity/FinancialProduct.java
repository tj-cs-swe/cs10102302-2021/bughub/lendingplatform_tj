package com.bughub.lendingplatform.entity;

import java.util.Date;

public class FinancialProduct {
    private Integer productId;

    private String productProvider;

    private String productCode;

    private String productName;

    private String productCurrency;

    private Float productAsset;

    private Float productNetValue;

    private Float productRiseOneMonth;

    private Float productRiseTwoMonth;

    private Float productRiseThreeMonth;

    private Float productRiseFourMonth;

    private Float productRiseFiveMonth;

    private Float productRiseSixMonth;

    private String productFundManager;

    private Date productCreateTime;

    public FinancialProduct(Integer productId, String productProvider, String productCode, String productName, String productCurrency, Float productAsset, Float productNetValue, Float productRiseOneMonth, Float productRiseTwoMonth, Float productRiseThreeMonth, Float productRiseFourMonth, Float productRiseFiveMonth, Float productRiseSixMonth, String productFundManager, Date productCreateTime) {
        this.productId = productId;
        this.productProvider = productProvider;
        this.productCode = productCode;
        this.productName = productName;
        this.productCurrency = productCurrency;
        this.productAsset = productAsset;
        this.productNetValue = productNetValue;
        this.productRiseOneMonth = productRiseOneMonth;
        this.productRiseTwoMonth = productRiseTwoMonth;
        this.productRiseThreeMonth = productRiseThreeMonth;
        this.productRiseFourMonth = productRiseFourMonth;
        this.productRiseFiveMonth = productRiseFiveMonth;
        this.productRiseSixMonth = productRiseSixMonth;
        this.productFundManager = productFundManager;
        this.productCreateTime = productCreateTime;
    }

    public FinancialProduct() {
        super();
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public String getProductProvider() {
        return productProvider;
    }

    public void setProductProvider(String productProvider) {
        this.productProvider = productProvider;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductCurrency() {
        return productCurrency;
    }

    public void setProductCurrency(String productCurrency) {
        this.productCurrency = productCurrency;
    }

    public Float getProductAsset() {
        return productAsset;
    }

    public void setProductAsset(Float productAsset) {
        this.productAsset = productAsset;
    }

    public Float getProductNetValue() {
        return productNetValue;
    }

    public void setProductNetValue(Float productNetValue) {
        this.productNetValue = productNetValue;
    }

    public Float getProductRiseOneMonth() {
        return productRiseOneMonth;
    }

    public void setProductRiseOneMonth(Float productRiseOneMonth) {
        this.productRiseOneMonth = productRiseOneMonth;
    }

    public Float getProductRiseTwoMonth() {
        return productRiseTwoMonth;
    }

    public void setProductRiseTwoMonth(Float productRiseTwoMonth) {
        this.productRiseTwoMonth = productRiseTwoMonth;
    }

    public Float getProductRiseThreeMonth() {
        return productRiseThreeMonth;
    }

    public void setProductRiseThreeMonth(Float productRiseThreeMonth) {
        this.productRiseThreeMonth = productRiseThreeMonth;
    }

    public Float getProductRiseFourMonth() {
        return productRiseFourMonth;
    }

    public void setProductRiseFourMonth(Float productRiseFourMonth) {
        this.productRiseFourMonth = productRiseFourMonth;
    }

    public Float getProductRiseFiveMonth() {
        return productRiseFiveMonth;
    }

    public void setProductRiseFiveMonth(Float productRiseFiveMonth) {
        this.productRiseFiveMonth = productRiseFiveMonth;
    }

    public Float getProductRiseSixMonth() {
        return productRiseSixMonth;
    }

    public void setProductRiseSixMonth(Float productRiseSixMonth) {
        this.productRiseSixMonth = productRiseSixMonth;
    }

    public String getProductFundManager() {
        return productFundManager;
    }

    public void setProductFundManager(String productFundManager) {
        this.productFundManager = productFundManager;
    }

    public Date getProductCreateTime() {
        return productCreateTime;
    }

    public void setProductCreateTime(Date productCreateTime) {
        this.productCreateTime = productCreateTime;
    }
}