import axios from 'axios'

axios.defaults.baseURL = 'http://119.45.246.113'

axios.defaults.timeout = 1000
axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=UTF-8'

axios.interceptors.request.use(
  config => {
    // console.log(config.data)
    // if (config.data === undefined) {
    //   throw Error('发送失败')
    // }
    console.log(config)
    return config
},
error => {
    console.log('发送失败')
    return Promise.error(error)
})

axios.interceptors.response.use(function (response) {
  // Any status code that lie within the range of 2xx cause this function to trigger
  // Do something with response data

  return response
}, function (error) {
  console.log('接受失败')
  return Promise.reject(error)
})
/**
 * post方法，对应post请求
 * @param {String} url [请求的url地址]
 * @param {Object} params [请求时携带的参数]
 */

 function get (url, params) {
    return new Promise((resolve, reject) => {
        axios.get(url, {
            params: params,
        }).then(res => {
            resolve(res.data)
        }).catch(err => {
            reject(err.data)
    })
  })
}
 function post (url, params) {
    return new Promise((resolve, reject) => {
        axios.post(url, params)
        .then(res => {
            resolve(res.data)
        })
        .catch(err => {
            reject(err.data)
        })
    })
  }

  export {
    get,
    post,
  }
