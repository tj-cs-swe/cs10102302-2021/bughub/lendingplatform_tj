package com.bughub.lendingplatform.entity;

public class Loan {
    private Integer loanId;

    private String loanProvider;

    private String loanCode;

    private String loanName;

    private String loanCurrency;

    private Float loanAsset;

    private Float loanAnnualInterest;

    private String loanAdditionalInfo;

    public Loan(Integer loanId, String loanProvider, String loanCode, String loanName, String loanCurrency, Float loanAsset, Float loanAnnualInterest, String loanAdditionalInfo) {
        this.loanId = loanId;
        this.loanProvider = loanProvider;
        this.loanCode = loanCode;
        this.loanName = loanName;
        this.loanCurrency = loanCurrency;
        this.loanAsset = loanAsset;
        this.loanAnnualInterest = loanAnnualInterest;
        this.loanAdditionalInfo = loanAdditionalInfo;
    }

    public Loan() {
        super();
    }

    public Integer getLoanId() {
        return loanId;
    }

    public void setLoanId(Integer loanId) {
        this.loanId = loanId;
    }

    public String getLoanProvider() {
        return loanProvider;
    }

    public void setLoanProvider(String loanProvider) {
        this.loanProvider = loanProvider;
    }

    public String getLoanCode() {
        return loanCode;
    }

    public void setLoanCode(String loanCode) {
        this.loanCode = loanCode;
    }

    public String getLoanName() {
        return loanName;
    }

    public void setLoanName(String loanName) {
        this.loanName = loanName;
    }

    public String getLoanCurrency() {
        return loanCurrency;
    }

    public void setLoanCurrency(String loanCurrency) {
        this.loanCurrency = loanCurrency;
    }

    public Float getLoanAsset() {
        return loanAsset;
    }

    public void setLoanAsset(Float loanAsset) {
        this.loanAsset = loanAsset;
    }

    public Float getLoanAnnualInterest() {
        return loanAnnualInterest;
    }

    public void setLoanAnnualInterest(Float loanAnnualInterest) {
        this.loanAnnualInterest = loanAnnualInterest;
    }

    public String getLoanAdditionalInfo() {
        return loanAdditionalInfo;
    }

    public void setLoanAdditionalInfo(String loanAddtionalInfo) {
        this.loanAdditionalInfo = loanAddtionalInfo;
    }
}