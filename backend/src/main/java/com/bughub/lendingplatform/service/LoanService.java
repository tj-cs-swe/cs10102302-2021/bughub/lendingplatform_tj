package com.bughub.lendingplatform.service;

import com.alibaba.fastjson.JSONArray;
import com.bughub.lendingplatform.entity.Loan;

import java.util.List;

public interface LoanService {
    List<Loan> getLoanInfo();

    Boolean addLoan(String userName, String loanCode, String loanName, String loanCurrency, Float loanAsset, Float loanAnnualInterest, String loanAdditionalInfo);

    Loan getSingleLoanInfo(Integer loanId);

    JSONArray getMyLoan(String userName);
}
