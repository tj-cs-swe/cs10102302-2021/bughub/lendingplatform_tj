package com.bughub.lendingplatform.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.bughub.lendingplatform.entity.Borrower;
import com.bughub.lendingplatform.entity.FinancialProduct;
import com.bughub.lendingplatform.entity.FinancialTrade;
import com.bughub.lendingplatform.entity.Lender;
import com.bughub.lendingplatform.mapper.BorrowerMapper;
import com.bughub.lendingplatform.mapper.FinancialProductMapper;
import com.bughub.lendingplatform.mapper.FinancialTradeMapper;
import com.bughub.lendingplatform.mapper.LenderMapper;
import com.bughub.lendingplatform.service.FinancialTradeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class FinancialTradeServiceImpl implements FinancialTradeService {
    @Qualifier("BorrowerMapper")
    @Autowired
    BorrowerMapper borrowerMapper;

    @Qualifier("LenderMapper")
    @Autowired
    LenderMapper lenderMapper;

    @Qualifier("FinancialProductMapper")
    @Autowired
    FinancialProductMapper financialProductMapper;

    @Qualifier("FinancialTradeMapper")
    @Autowired
    FinancialTradeMapper financialTradeMapper;

    @Override
    public boolean addNewTrade(String borrowerName, String lenderName, Integer productId, Float amount, String bankCard){
        Borrower b = borrowerMapper.selectByPrimaryKey(borrowerName);

        if (b == null)
            return false;

        Lender l = lenderMapper.selectByPrimaryKey(lenderName);

        if (l == null)
            return false;

        FinancialProduct fp = financialProductMapper.selectByPrimaryKey(productId);

        if (fp == null || !fp.getProductProvider().equals(l.getUserName()))
            return false;

        FinancialTrade ft = new FinancialTrade();
        ft.setBorrowerName(borrowerName);
        ft.setAmount(amount);
        ft.setBankcard(bankCard);
        ft.setLenderName(lenderName);
        ft.setIsValid(false);
        ft.setTradeCreateTime(new Date());
        ft.setProductId(productId);

        financialTradeMapper.insert(ft);

        return true;
    }

    @Override
    public List<FinancialTrade> getTradeByLenderName(String lenderName){
        List<FinancialTrade> data = financialTradeMapper.selectByLenderName(lenderName);
        List<FinancialTrade> res = new LinkedList<>();

        for (FinancialTrade datum : data) {
            if (!datum.getIsValid())
                res.add(datum);
        }

        return res;
    }

    @Override
    public Object updateIsValid(int tradeInfoId, Boolean isValid){

        JSONObject res = new JSONObject();
        String status;
        if(isValid) {
            FinancialTrade financialTrade;
            financialTrade = financialTradeMapper.selectByPrimaryKey(tradeInfoId);
            if(financialTrade == null) {
                status = "404";
                res.put("status",status);
                return res;
            }
            else if(financialTrade.getIsValid()){
                status = "404";
                res.put("status",status);
                return res;
            }
            else{
                financialTradeMapper.updateIsValidByPrimaryKey(tradeInfoId,1);
                Date tradeValidTime = new Date();
                financialTradeMapper.updateDateValidTime(tradeInfoId,tradeValidTime);
                status = "200";
                res.put("status",status);
                return res;
            }

        }
        else{
            status = "404";
            res.put("status",status);
            return res;
        }

    }
}
