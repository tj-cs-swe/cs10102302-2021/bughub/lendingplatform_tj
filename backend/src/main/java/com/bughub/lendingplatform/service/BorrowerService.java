package com.bughub.lendingplatform.service;

import com.alibaba.fastjson.JSONArray;

public interface BorrowerService {
    Object getBorrowerInfoByManager(String userName);
    Object updateRankLevelByManager(String userName, Boolean isValid);
    JSONArray getRankLevelOneBorrowerByManager();
}
