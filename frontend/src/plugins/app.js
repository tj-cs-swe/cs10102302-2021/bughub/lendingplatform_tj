/**
 * plugins/app.js
 * 可以理解为是自动加载然后触发，把这些组件挨个注册（全局注册?)
 * Automatically loads and bootstraps files
 * in the `./src/components/` folder.
 */

// Imports
import Vue from 'vue'

const requireComponent = require.context('@/components', true, /\.vue$/)

for (const file of requireComponent.keys()) {
  const componentConfig = requireComponent(file)
  Vue.component(
    componentConfig.default.name, // 组件名
    componentConfig.default || componentConfig, // 组件内置
  )
}

// require.context:wepack下API，可以实现自动化导入模块。
