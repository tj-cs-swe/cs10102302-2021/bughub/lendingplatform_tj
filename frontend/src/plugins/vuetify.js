// Vuetify Documentation https://vuetifyjs.com

import Vue from 'vue'
import Vuetify from 'vuetify/lib/framework'
import ripple from 'vuetify/lib/directives/ripple'

Vue.use(Vuetify, { directives: { ripple } }) // 设置了波纹指令，

const theme = {
  primary: '#E91E63',
  secondary: '#9C27b0',
  accent: '#e91e63',
  info: '#00CAE3',
  success: '#4CAF50',
  warning: '#FB8C00',
  error: '#FF5252',
}

// 最后导出一个Vuetify对象
export default new Vuetify({
  breakpoint: { mobileBreakpoint: 960 }, // breakpoint的临界点是960,当大于960之后就会产生不一样的页面结果。
  icons: {
    values: { expand: 'mdi-menu-down' }, // 默认使用的就是meterial design icons 但不太确定这个expand的含义
  },
  theme: {
    themes: {
      dark: theme,
      light: theme,
    },
  },
})
