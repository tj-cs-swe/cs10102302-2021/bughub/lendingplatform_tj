package com.bughub.lendingplatform.service;

import com.bughub.lendingplatform.entity.User;

public interface UserService {
    User[] getAllInactiveUser();
    String addUser(String username, String password, int usertype, String usercode);
    String auditRegister(String username,Boolean isValid);
    String updateEnterpriseInfo(String username,String usercode,String borrowerName,String borrower_telephone,String borrower_address,String borrower_email,String borrower_link);
    String updateLenderInfo(String username,String usercode,String lenderName,String lender_telephone,String lender_address,String lender_email,String lender_link);
    User getUser(String username, String password);
}
