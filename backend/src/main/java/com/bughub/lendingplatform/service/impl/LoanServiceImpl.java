package com.bughub.lendingplatform.service.impl;


import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.bughub.lendingplatform.entity.*;
import com.bughub.lendingplatform.mapper.*;
import com.bughub.lendingplatform.service.LoanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class LoanServiceImpl implements LoanService {

    @Qualifier("LoanMapper")
    @Autowired
    LoanMapper loanMapper;

    @Override
    public List<Loan> getLoanInfo(){

        List<Loan> loans = loanMapper.getAllLoan();
        return loans;
    }

    @Override
    public Boolean addLoan(String userName, String loanCode, String loanName, String loanCurrency, Float loanAsset, Float loanAnnualInterest, String loanAdditionalInfo){

        Lender lender = lenderMapper.selectByPrimaryKey(userName);

        if (lender == null)
            return false;

        if (!loanCurrency.equals("人民币"))
            return false;
        //用户存在则插入成功，不检查名字重复性

        Loan loan = new Loan();
        loan.setLoanProvider(userName);
        loan.setLoanCode(loanCode);
        loan.setLoanName(loanName);
        loan.setLoanCurrency(loanCurrency);
        loan.setLoanAsset(loanAsset);
        loan.setLoanAnnualInterest(loanAnnualInterest);
        loan.setLoanAdditionalInfo(loanAdditionalInfo);

        return loanMapper.insert(loan) == 1;
    }

    @Override
    public Loan getSingleLoanInfo(Integer loanId){
        Loan loan;
        loan = loanMapper.selectByPrimaryKey(loanId);

        return loan;
    }

    @Qualifier("BorrowerMapper")
    @Autowired
    BorrowerMapper borrowerMapper;

    @Qualifier("LoanTradeMapper")
    @Autowired
    LoanTradeMapper loanTradeMapper;


    @Qualifier("LenderMapper")
    @Autowired
    LenderMapper lenderMapper;


    @Override
    public JSONArray getMyLoan(String userName){
        Borrower borrower = borrowerMapper.selectByPrimaryKey(userName);
        Lender lender = lenderMapper.selectByPrimaryKey(userName);
        //FinancialProduct financialProduct;
        //List<FinancialTrade> financialTradeList;
        List<LoanTrade> loanTradeList;
        //List<FinancialProduct> financialProductList;
        JSONArray res = new JSONArray();
        //FinancialProduct financialProduct;
        Loan loan;
        if(borrower != null)
        {
            Integer productId;
            loanTradeList = loanTradeMapper.selectByBorrowerName(userName);
            for(int i=0;i<loanTradeList.size();i++){
                productId = loanTradeList.get(i).getLoanId();
                loan = loanMapper.selectByPrimaryKey(productId);
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("loanId", loan.getLoanId());
                jsonObject.put("loanProvider", loan.getLoanProvider());
                jsonObject.put("loanCode", loan.getLoanCode());
                jsonObject.put("loanName", loan.getLoanName());
                jsonObject.put("loanCurrency", loan.getLoanCurrency());
                jsonObject.put("loanAsset", loan.getLoanAsset());
                jsonObject.put("loanAnnualInterest", loan.getLoanAnnualInterest());
                jsonObject.put("loanAdditionalInfo", loan.getLoanAdditionalInfo());
                jsonObject.put("isValid", loanTradeList.get(i).getIsValid());
                res.add(jsonObject);

            }
            if(loanTradeList.size() > 0){
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("status", "200");

                res.add(jsonObject);
            }
            else{
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("status", "404");

                res.add(jsonObject);
            }
            return res;
        }
        else if(lender != null){
            List<Loan> loanList = loanMapper.selectByLoanProvider(userName);
            if(loanList.size() == 0){
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("status", "404");

                res.add(jsonObject);

                return res;
            }
            else{
                for(int i=0;i<loanList.size();i++){
                    JSONObject jsonObject = new JSONObject();
                    loan = loanList.get(i);
                    jsonObject.put("loanId", loan.getLoanId());
                    jsonObject.put("loanProvider", loan.getLoanProvider());
                    jsonObject.put("loanCode", loan.getLoanCode());
                    jsonObject.put("loanName", loan.getLoanName());
                    jsonObject.put("loanCurrency", loan.getLoanCurrency());
                    jsonObject.put("loanAsset", loan.getLoanAsset());
                    jsonObject.put("loanAnnualInterest", loan.getLoanAnnualInterest());
                    jsonObject.put("loanAdditionalInfo", loan.getLoanAdditionalInfo());
                    res.add(jsonObject);
                }
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("status", "200");

                res.add(jsonObject);

                return res;
            }
        }
        else{
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("status", "404");

            res.add(jsonObject);

            return res;
        }
    }
}
