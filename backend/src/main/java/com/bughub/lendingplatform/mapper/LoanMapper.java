package com.bughub.lendingplatform.mapper;

import com.bughub.lendingplatform.entity.FinancialProduct;
import com.bughub.lendingplatform.entity.Loan;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

import java.util.List;

@Mapper
@Component("LoanMapper")
public interface LoanMapper {
    int deleteByPrimaryKey(Integer loanId);

    int insert(Loan record);

    int insertSelective(Loan record);

    Loan selectByPrimaryKey(Integer loanId);

    int updateByPrimaryKeySelective(Loan record);

    int updateByPrimaryKeyWithBLOBs(Loan record);

    int updateByPrimaryKey(Loan record);

    List<Loan> getAllLoan();

    List<Loan> selectByLoanProvider(String loanProvider);


}