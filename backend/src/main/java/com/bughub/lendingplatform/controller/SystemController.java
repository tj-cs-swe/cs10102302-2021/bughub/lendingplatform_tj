package com.bughub.lendingplatform.controller;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.JSONArray;
import com.bughub.lendingplatform.entity.*;
import com.bughub.lendingplatform.service.impl.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;


@CrossOrigin
@Controller
public class SystemController {

    @Autowired
    UserServiceImpl userService;

    @RequestMapping(value="login")
    @ResponseBody
    public Object login(@RequestParam("userName")String username,
                     @RequestParam("password")String password){
        User user = userService.getUser(username, password);

        //status，成功登录为true，否则为false
        String status;
        if (user != null)
            status = "200";
        else
            status = "404";

        JSONObject res = new JSONObject();
        res.put("status", status);

        //避免user == null的问题
        if (status.equals("200"))
            res.put("userType", user.getUserType());
        else
            res.put("userType", -1);

        return res;
    }

    @RequestMapping(value = { "register" }, produces="application/json;charset=UTF-8")
    @ResponseBody
    public Object register(@RequestParam("userName")String username,
                           @RequestParam("password")String password,
                           @RequestParam("userType")int usertype,
                           @RequestParam("userCode")String usercode
    ){
        String status=userService.addUser(username,password,usertype,usercode);
        JSONObject res = new JSONObject();
        res.put("status", status);
        return res;

    }

    @RequestMapping(value = { "showRegisters" }, produces="application/json;charset=UTF-8")
    @ResponseBody
    public JSONArray showRegisters(
    ){

        User[] allUser=userService.getAllInactiveUser();
        JSONArray res = new JSONArray();
        for(int i=0;i<allUser.length;i++)
        {
            JSONObject temp = new JSONObject();
            temp.put("userName", allUser[i].getUserName());
            temp.put("userType", allUser[i].getUserType());
            temp.put("userCode", allUser[i].getUserCode());
            res.add(temp);
        }
        return res;
    }
    @RequestMapping(value = { "auditRegisters" }, produces="application/json;charset=UTF-8")
    @ResponseBody
    public Object auditRegister(@RequestParam("userName")String username,
                           @RequestParam("isValid")Boolean isValid
    ){
        String status=userService.auditRegister(username,isValid);
        JSONObject res = new JSONObject();
        res.put("status", status);
        return res;

    }
    @RequestMapping(value = { "manageEnterpriseInfo" }, produces="application/json;charset=UTF-8")
    @ResponseBody
    public Object manageEnterpriseInfo(
            @RequestParam("userName")String username,
            @RequestParam("userCode")String usercode,
            @RequestParam("borrowerName")String borrowerName,
            @RequestParam("borrowerTelephone")String borrower_telephone,
            @RequestParam("borrowerAddress")String borrower_address,
            @RequestParam("borrowerEmail")String borrower_email,
            @RequestParam("borrowerLink")String borrower_link
    ){
        String status=userService.updateEnterpriseInfo(username,usercode,borrowerName,borrower_telephone,borrower_address,borrower_email,borrower_link);
        JSONObject res = new JSONObject();
        res.put("status", status);
        return res;
    }
    @Autowired
    BorrowerServiceImpl borrowerService;

    @RequestMapping(value = { "showEnterprisesInfo" }, produces="application/json;charset=UTF-8")
    @ResponseBody
    public Object showEnterprisesInfo(@RequestParam("userName")String userName
    ){
        return borrowerService.getBorrowerInfoByManager(userName);
    }


    @RequestMapping(value = { "auditEnterprises" }, produces="application/json;charset=UTF-8")
    @ResponseBody
    public Object auditEnterprises(@RequestParam("userName")String userName,
                                   @RequestParam("isValid")Boolean isValid
    ){
        return borrowerService.updateRankLevelByManager(userName, isValid);
    }

    @RequestMapping(value = { "showEnterprises" }, produces="application/json;charset=UTF-8")
    @ResponseBody
    public JSONArray showEnterprises(){
        return borrowerService.getRankLevelOneBorrowerByManager();
    }

    @Autowired
    FinancialProductServiceImpl financialProductService;

    @RequestMapping(value = { "showProducts" }, produces="application/json;charset=UTF-8")
    @ResponseBody
    public List<FinancialProduct> showProducts(){
        return financialProductService.getAllProduct();
    }

    @Autowired
    FinancialTradeServiceImpl financialTradeService;

    @RequestMapping(value = { "purchaseProduct" }, produces="application/json;charset=UTF-8")
    @ResponseBody
    public Object purchaseProduct(@RequestParam("borrowerName")String borrowerName,
                                  @RequestParam("lenderName")String lenderName,
                                  @RequestParam("productId")Integer productId,
                                  @RequestParam("amount")Float amount,
                                  @RequestParam("bankCard")String bankCard
                                  ){
        boolean status = financialTradeService.addNewTrade(borrowerName, lenderName, productId, amount, bankCard);

        String res;

        if (status)
            res = "200";
        else
            res = "404";

        JSONObject object = new JSONObject();

        object.put("status", res);

        return object;
    }

    @RequestMapping(value = { "showProductPurchase" }, produces="application/json;charset=UTF-8")
    @ResponseBody
    public JSONArray showProductPurchase(@RequestParam("userName")String lenderName){
        List<FinancialTrade> financialTradeList = financialTradeService.getTradeByLenderName(lenderName);

        JSONArray jsonArray = new JSONArray();
        for (int i = 0; i < financialTradeList.size(); ++i) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("tradeInfoId", financialTradeList.get(i).getTradeInfoId());
            jsonObject.put("productId", financialTradeList.get(i).getLenderName());
            jsonObject.put("amount", financialTradeList.get(i).getAmount());
            jsonObject.put("bankCard", financialTradeList.get(i).getBankcard());
            jsonObject.put("tradeCreateTime", financialTradeList.get(i).getTradeCreateTime());

            jsonArray.add(jsonObject);
        }
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("status", "200");

        jsonArray.add(jsonObject);

        return jsonArray;
    }



    @RequestMapping(value={"confirmProductPurchase"}, produces="application/json;charset=UTF-8")
    @ResponseBody
    public Object confirmProductPurchase(@RequestParam("tradeInfoId")Integer tradeInfoId,
                                         @RequestParam("isValid")Boolean isValid
    ){
        return financialTradeService.updateIsValid(tradeInfoId,isValid);
    }



    @Autowired
    LoanServiceImpl loanService;

    @RequestMapping(value="showLoans",produces="application/json;charset=UTF-8")
    @ResponseBody
    public JSONArray showLoans(){
        List<Loan> loans = loanService.getLoanInfo();

        JSONArray res = new JSONArray();
        for (int i = 0; i < loans.size(); ++i) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("loanId", loans.get(i).getLoanId());
            jsonObject.put("loanProvider", loans.get(i).getLoanProvider());
            jsonObject.put("loanCode", loans.get(i).getLoanCode());
            jsonObject.put("loanName", loans.get(i).getLoanName());
            jsonObject.put("loanCurrency", loans.get(i).getLoanCurrency());
            jsonObject.put("loanAsset", loans.get(i).getLoanAsset());
            jsonObject.put("loanAnnualInterest", loans.get(i).getLoanAnnualInterest());
            jsonObject.put("loanAdditionalInfo", loans.get(i).getLoanAdditionalInfo());
            res.add(jsonObject);
        }
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("status", "200");

        res.add(jsonObject);

        return res;
    }

    @Autowired
    LoanTradeServiceImpl loanTradeService;

    @RequestMapping(value = { "purchaseLoan" }, produces="application/json;charset=UTF-8")
    @ResponseBody
    public Object purchaseLoan(@RequestParam("borrowerName")String borrowerName,
                                  @RequestParam("lenderName")String lenderName,
                                  @RequestParam("loanId")Integer loanId,
                                  @RequestParam("amount")Float amount,
                                  @RequestParam("bankCard")String bankCard
    ){
        boolean status = loanTradeService.addNewLoanTrade(borrowerName, lenderName, loanId, amount, bankCard);

        String res;

        if (status)
            res = "200";
        else
            res = "404";

        JSONObject object = new JSONObject();

        object.put("status", res);

        return object;
    }

    @Autowired
    MessageServiceImpl messageService;

    @RequestMapping(value = { "sendMessage" }, produces="application/json;charset=UTF-8")
    @ResponseBody
    public Object sendMessage(@RequestParam("messageSender")String messageSender,
                              @RequestParam("messageReceiver")String messageReceiver,
                              @RequestParam("messageTitle")String messageTitle,
                              @RequestParam("messageText")String messageText){

        String status;
        if (messageService.sendMessage(messageSender, messageReceiver, messageTitle, messageText))
            status = "200";
        else
            status = "404";

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("status", status);
        return jsonObject;
    }

    @RequestMapping(value = { "getMessageSent" }, produces="application/json;charset=UTF-8")
    @ResponseBody
    public JSONArray getMessageSent(@RequestParam("messageSender")String messageSender){
        List<Message> messageList = messageService.getMessageSent(messageSender);

        JSONArray jsonArray = new JSONArray();

        for (Message message : messageList) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("messageSender", message.getMessageSender());
            jsonObject.put("messageReceiver", message.getMessageReceiver());
            jsonObject.put("messageTitle", message.getMessageTitle());
            jsonObject.put("messageText", message.getMessageText());
            jsonObject.put("messageSendTime", message.getMessageSendTime());

            jsonArray.add(jsonObject);
        }

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("status", "200");

        jsonArray.add(jsonObject);
        return jsonArray;
    }

    @RequestMapping(value = { "getMessageReceived" }, produces="application/json;charset=UTF-8")
    @ResponseBody
    public JSONArray getMessageReceived(@RequestParam("messageReceiver")String messageReceiver){
        List<Message> messageList = messageService.getMessageReceived(messageReceiver);

        JSONArray jsonArray = new JSONArray();

        for (Message message : messageList) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("messageSender", message.getMessageSender());
            jsonObject.put("messageReceiver", message.getMessageReceiver());
            jsonObject.put("messageTitle", message.getMessageTitle());
            jsonObject.put("messageText", message.getMessageText());
            jsonObject.put("messageSendTime", message.getMessageSendTime());

            jsonArray.add(jsonObject);
        }

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("status", "200");

        jsonArray.add(jsonObject);
        return jsonArray;
    }

    @RequestMapping(value={"showLoanPurchase"},produces="application/json;charset=UTF-8")
    @ResponseBody
    public JSONArray showLoanPurchase(@RequestParam("userName")String userName){
        List<LoanTrade> loanTradeList = loanTradeService.showLoanPurchase(userName);

        JSONArray res = new JSONArray();
        for (int i = 0; i < loanTradeList.size(); ++i) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("tradeInfoId", loanTradeList.get(i).getTradeInfoId());
            jsonObject.put("LoanId", loanTradeList.get(i).getLoanId());
            jsonObject.put("amount", loanTradeList.get(i).getAmount());
            jsonObject.put("bankCard", loanTradeList.get(i).getBankCard());
            jsonObject.put("tradeCreateTime", loanTradeList.get(i).getTradeCreateTime());

            res.add(jsonObject);
        }
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("status", "200");

        res.add(jsonObject);

        return res;
    }

    @RequestMapping(value={"confirmLoanPurchase"}, produces="application/json;charset=UTF-8")
    @ResponseBody
    public Object confirmLoanPurchase(@RequestParam("tradeInfoId")Integer tradeInfoId,
                                         @RequestParam("isValid")Boolean isValid
    ){
        return loanTradeService.updateIsValid(tradeInfoId,isValid);
    }



    @RequestMapping(value={"publishProduct"},produces="application/json;charset=UTF-8")
    @ResponseBody
    public Object publishProduct(@RequestParam("userName")String userName,
                                 @RequestParam("productCode")String productCode,
                                 @RequestParam("productName")String productName,
                                 @RequestParam("productCurrency")String productCurrency,
                                 @RequestParam("productAsset")Float productAsset,
                                 @RequestParam("productNetValue")Float productNetValue,
                                 @RequestParam("productFundManager")String productFundManager
    ){
        JSONArray res = new JSONArray();
        if(financialProductService.addProduct(userName, productCode, productName, productCurrency, productAsset, productNetValue, productFundManager)){
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("status", "200");

            res.add(jsonObject);

            return res;
        }
        else{
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("status", "404");

            res.add(jsonObject);

            return res;
        }
    }

    @RequestMapping(value={"publishLoan"},produces="application/json;charset=UTF-8")
    @ResponseBody
    public Object publishLoan(@RequestParam("userName")String userName,
                                 @RequestParam("loanCode")String loanCode,
                                 @RequestParam("loanName")String loanName,
                                 @RequestParam("loanCurrency")String loanCurrency,
                                 @RequestParam("loanAsset")Float loanAsset,
                                 @RequestParam("loanAnnualInterest")Float loanAnnualInterest,
                                 @RequestParam("loanAdditionalInfo")String loanAdditionalInfo
    ){
        JSONArray res = new JSONArray();
        if(loanService.addLoan(userName, loanCode, loanName, loanCurrency, loanAsset, loanAnnualInterest, loanAdditionalInfo)){
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("status", "200");

            res.add(jsonObject);

            return res;
        }
        else{
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("status", "404");

            res.add(jsonObject);

            return res;
        }
    }

    @RequestMapping(value="showSingleLoan",produces="application/json;charset=UTF-8")
    @ResponseBody
    public Object showSingleLoan(@RequestParam("loanId")Integer loanId){
        Loan loan = loanService.getSingleLoanInfo(loanId);
        JSONObject jsonObject = new JSONObject();
        if(loan == null){

            jsonObject.put("status", "404");
            return jsonObject;
        }
        else{
            jsonObject.put("loanId", loan.getLoanId());
            jsonObject.put("loanProvider", loan.getLoanProvider());
            jsonObject.put("loanCode", loan.getLoanCode());
            jsonObject.put("loanName", loan.getLoanName());
            jsonObject.put("loanCurrency", loan.getLoanCurrency());
            jsonObject.put("loanAsset", loan.getLoanAsset());
            jsonObject.put("loanAnnualInterest", loan.getLoanAnnualInterest());
            jsonObject.put("loanAdditionalInfo", loan.getLoanAdditionalInfo());
            jsonObject.put("status", "200");


            return jsonObject;
        }

    }


    @RequestMapping(value="showSingleProduct",produces="application/json;charset=UTF-8")
    @ResponseBody
    public Object showSingleProduct(@RequestParam("productId")Integer productId){

        FinancialProduct financialProduct = financialProductService.getSingleProduct(productId);
        JSONObject jsonObject = new JSONObject();
        if(financialProduct == null){

            jsonObject.put("status", "404");
            return jsonObject;
        }
        else{

            jsonObject.put("productId", financialProduct.getProductId());
            jsonObject.put("productProvider", financialProduct.getProductProvider());
            jsonObject.put("productCode", financialProduct.getProductCode());
            jsonObject.put("productName", financialProduct.getProductName());
            jsonObject.put("productCurrency", financialProduct.getProductCurrency());
            jsonObject.put("productAsset", financialProduct.getProductAsset());
            jsonObject.put("productNetValue", financialProduct.getProductNetValue());
            jsonObject.put("productRiseOneMonth", financialProduct.getProductRiseOneMonth());
            jsonObject.put("productRiseTwoMonth", financialProduct.getProductRiseTwoMonth());
            jsonObject.put("productRiseThreeMonth", financialProduct.getProductRiseThreeMonth());
            jsonObject.put("productRiseFourMonth", financialProduct.getProductRiseFourMonth());
            jsonObject.put("productRiseFiveMonth", financialProduct.getProductRiseFiveMonth());
            jsonObject.put("productRiseSixMonth", financialProduct.getProductRiseSixMonth());
            jsonObject.put("productFundManager", financialProduct.getProductFundManager());
            jsonObject.put("productCreateTime", financialProduct.getProductCreateTime());

            jsonObject.put("status", "200");



            return jsonObject;
        }

    }


    @RequestMapping(value = {"showMyProducts"},produces="application/json;charset=UTF-8")
    @ResponseBody
    public JSONArray showMyProject(@RequestParam("userName")String userName){
        return financialProductService.getMyProduct(userName);
    }


    @RequestMapping(value = {"showMyLoans"},produces="application/json;charset=UTF-8")
    @ResponseBody
    public JSONArray showMyLoan(@RequestParam("userName")String userName){
        return loanService.getMyLoan(userName);
    }

    @RequestMapping(value = { "manageInstitutionInfo" }, produces="application/json;charset=UTF-8")
    @ResponseBody
    public Object manageInstitutionInfo(
            @RequestParam("userName")String username,
            @RequestParam("userCode")String usercode,
            @RequestParam("lenderName")String lenderName,
            @RequestParam("lenderTelephone")String lender_telephone,
            @RequestParam("lenderAddress")String lender_address,
            @RequestParam("lenderEmail")String lender_email,
            @RequestParam("lenderLink")String lender_link
    ){
        String status=userService.updateLenderInfo(username,usercode,lenderName,lender_telephone,lender_address,lender_email,lender_link);
        JSONObject res = new JSONObject();
        res.put("status", status);
        return res;
    }



}

