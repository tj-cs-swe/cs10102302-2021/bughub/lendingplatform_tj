package com.bughub.lendingplatform.entity;

import java.util.Date;

public class LoanTrade {
    private Integer tradeInfoId;

    private String borrowerName;

    private String lenderName;

    private Integer loanId;

    private Float amount;

    private String bankCard;

    private Date tradeCreateTime;

    private Boolean isValid;

    private Date tradeValidTime;

    public LoanTrade(Integer tradeInfoId, String borrowerName, String lenderName, Integer loanId, Float amount, String bankCard, Date tradeCreateTime, Boolean isValid, Date tradeValidTime) {
        this.tradeInfoId = tradeInfoId;
        this.borrowerName = borrowerName;
        this.lenderName = lenderName;
        this.loanId = loanId;
        this.amount = amount;
        this.bankCard = bankCard;
        this.tradeCreateTime = tradeCreateTime;
        this.isValid = isValid;
        this.tradeValidTime = tradeValidTime;
    }

    public LoanTrade() {
        super();
    }

    public Integer getTradeInfoId() {
        return tradeInfoId;
    }

    public void setTradeInfoId(Integer tradeInfoId) {
        this.tradeInfoId = tradeInfoId;
    }

    public String getBorrowerName() {
        return borrowerName;
    }

    public void setBorrowerName(String borrowerName) {
        this.borrowerName = borrowerName;
    }

    public String getLenderName() {
        return lenderName;
    }

    public void setLenderName(String lenderName) {
        this.lenderName = lenderName;
    }

    public Integer getLoanId() {
        return loanId;
    }

    public void setLoanId(Integer loanId) {
        this.loanId = loanId;
    }

    public Float getAmount() {
        return amount;
    }

    public void setAmount(Float amount) {
        this.amount = amount;
    }

    public String getBankCard() {
        return bankCard;
    }

    public void setBankCard(String bankCard) {
        this.bankCard = bankCard;
    }

    public Date getTradeCreateTime() {
        return tradeCreateTime;
    }

    public void setTradeCreateTime(Date tradeCreateTime) {
        this.tradeCreateTime = tradeCreateTime;
    }

    public Boolean getIsValid() {
        return isValid;
    }

    public void setIsValid(Boolean isValid) {
        this.isValid = isValid;
    }

    public Date getTradeValidTime() {
        return tradeValidTime;
    }

    public void setTradeValidTime(Date tradeValidTime) {
        this.tradeValidTime = tradeValidTime;
    }
}