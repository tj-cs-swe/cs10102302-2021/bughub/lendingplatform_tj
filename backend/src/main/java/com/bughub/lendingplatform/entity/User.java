package com.bughub.lendingplatform.entity;

public class User {
    private String userName;

    private String password;

    private int userType;

    private String userCode;

    public User(String userName, String password, int userType, String userCode) {
        this.userName = userName;
        this.password = password;
        this.userType = userType;
        this.userCode = userCode;
    }

    public User() {
        super();
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getUserType() {
        return userType;
    }

    public void setUserType(int userType) {
        this.userType = userType;
    }

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }
}