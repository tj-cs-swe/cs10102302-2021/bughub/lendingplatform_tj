package com.bughub.lendingplatform.entity;

import java.util.Date;

public class FinancialTrade {
    private Integer tradeInfoId;

    private String borrowerName;

    private String lenderName;

    private Integer productId;

    private Float amount;

    private String bankcard;

    private Date tradeCreateTime;

    private Boolean isValid;

    private Date tradeValidTime;

    public FinancialTrade(Integer tradeInfoId, String borrowerName, String lenderName, Integer productId, Float amount, String bankcard, Date tradeCreateTime, Boolean isValid, Date tradeValidTime) {
        this.tradeInfoId = tradeInfoId;
        this.borrowerName = borrowerName;
        this.lenderName = lenderName;
        this.productId = productId;
        this.amount = amount;
        this.bankcard = bankcard;
        this.tradeCreateTime = tradeCreateTime;
        this.isValid = isValid;
        this.tradeValidTime = tradeValidTime;
    }

    public FinancialTrade() {
        super();
    }

    public Integer getTradeInfoId() {
        return tradeInfoId;
    }

    public void setTradeInfoId(Integer tradeInfoId) {
        this.tradeInfoId = tradeInfoId;
    }

    public String getBorrowerName() {
        return borrowerName;
    }

    public void setBorrowerName(String borrowerName) {
        this.borrowerName = borrowerName;
    }

    public String getLenderName() {
        return lenderName;
    }

    public void setLenderName(String lenderName) {
        this.lenderName = lenderName;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public Float getAmount() {
        return amount;
    }

    public void setAmount(Float amount) {
        this.amount = amount;
    }

    public String getBankcard() {
        return bankcard;
    }

    public void setBankcard(String bankcard) {
        this.bankcard = bankcard;
    }

    public Date getTradeCreateTime() {
        return tradeCreateTime;
    }

    public void setTradeCreateTime(Date tradeCreateTime) {
        this.tradeCreateTime = tradeCreateTime;
    }

    public Boolean getIsValid() {
        return isValid;
    }

    public void setIsValid(Boolean isValid) {
        this.isValid = isValid;
    }

    public Date getTradeValidTime() {
        return tradeValidTime;
    }

    public void setTradeValidTime(Date tradeValidTime) {
        this.tradeValidTime = tradeValidTime;
    }
}