package com.bughub.lendingplatform.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.bughub.lendingplatform.entity.User;
import com.bughub.lendingplatform.mapper.UserMapper;
import com.bughub.lendingplatform.service.UserService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    @Qualifier("userMapper")
    @Autowired
    UserMapper userMapper;

    @Override
    public User getUser(String username, String password) {
        return userMapper.getUser(username, password);
    }

    @Override
    public User[] getAllInactiveUser() {
        User[] allUser=new User[10000];
        allUser=userMapper.getAllInactiveUser();
        return allUser;
    }
    @Override
    public String auditRegister(String username,Boolean isValid){
        Boolean Registered=userMapper.checkLenderRegistered(username)||userMapper.checkBorrowerRegistered(username);
        if(!Registered)
        {
            Boolean Exist=userMapper.checkUserExists(username);
            if(Exist)
            {
                if(userMapper.getUserType(username)==1)
                    userMapper.registerUserBorrower(username);
                else
                    userMapper.registerUserLender(username);
                return "200";//正常完成注册
            }
            else
            {
                return "404";//没有这个用户
            }

        }
        else
            return "200";  //已经注册过了
    }
    @Override
    public String addUser(String username, String password,int usertype , String usercode) {
        String status;

        if(!userMapper.checkUserExists(username)&&(usertype<3)&&(usertype>0))
        {
            userMapper.addUser(username,password,usertype,usercode);
            status = "200";
        }
        else
            status = "404";

        return status;
//        return userMapper.addUser(username,password,usertype,usercode);
    }

    @Override
    public String updateEnterpriseInfo(String username,String usercode,String borrowerName,String borrower_telephone,String borrower_address,String borrower_email,String borrower_link)
    {
        userMapper.updateEnterpriseInfo(username,usercode,borrowerName,borrower_telephone,borrower_address,borrower_email,borrower_link);
        return "200";
    }

    @Override
    public String updateLenderInfo(String username,String usercode,String lenderName,String lender_telephone,String lender_address,String lender_email,String lender_link)
    {
        userMapper.updateLenderInfo(username,usercode,lenderName,lender_telephone,lender_address,lender_email,lender_link);
        return "200";
    }

}
