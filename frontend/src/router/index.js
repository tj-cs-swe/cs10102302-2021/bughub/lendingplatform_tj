// Imports
import Vue from 'vue'
import Router from 'vue-router'
import { trailingSlash } from '@/util/helpers'
import {
  layout,
  route,
} from '@/util/routes'
import { getToken } from '@/util/cookie'
import { authenRouter } from './authenticate'
// import './authenticate'

Vue.use(Router)
const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  scrollBehavior: (to, from, savedPosition) => {
    if (to.hash) return { selector: to.hash }
    if (savedPosition) return savedPosition

    return { x: 0, y: 0 }
  },
  routes: [
    layout('Default', [
      route('Dashboard'),

      // Pages
      route('UserProfile', null, 'components/profile'),

      // Components
      route('Notifications', null, 'components/notifications'),
      route('Icons', null, 'components/icons'),
      route('Typography', null, 'components/typography'),

      // Tables
      route('Regular Tables', null, 'tables/regular'),

      // Maps
       route('Google Maps', null, 'maps/google'),

       // mainPage
       route('Mainpage', null, 'mainpage'),

       // showProducts
       route('ShowProducts', null, 'showProducts'),
       route('ShowMyProducts', null, 'showMyProducts'),

       route('SuperUser/JudgeRegister', null, 'judge/register'),
       route('SuperUser/JudgeApplying', null, 'judge/applying'),

       route('ShowEnterprise', null, 'showEnterprises'),

       route('EnterpriseInformation', null, 'EnterpriseInformation'),

       route('productsInformation', null, 'productsInformation'),

      //  route('ShowLoans', null, 'showLoans'),
      //  route('ShowMyLoans', null, 'showMyLoans'),

       route('purchaseLoan', null, 'purchaseLoan'),

       // pages for finance
       route('FinancialUser/FinancialProducts', null, 'financial/products'),
       route('FinancialUser/FinancialProductDetail', null, 'financial/productDetail'),
       route('FinancialUser/FinancialLoans', null, 'financial/loans'),
       route('FinancialUser/FinancialLoanDetail', null, 'financial/loanDetail'),
       route('FinancialUser/FinancialSendMessage', null, 'financial/sendmessage'),
       route('FinancialUser/FinancialSendLoan', null, 'financial/sendloan'),
       route('FinancialUser/FinancialSendProduct', null, 'financial/sendProduct'),

       // pages for enterprises
       route('EnterpriseUser/EnterpriseProducts', null, 'Enterprise/products'),
       route('EnterpriseUser/EnterpriseMyProducts', null, 'Enterprise/myproducts'),
       route('EnterpriseUser/EnterpriseProductDetail', null, 'Enterprise/productDetail'),
       route('EnterpriseUser/EnterpriseLoans', null, 'Enterprise/loans'),
       route('EnterpriseUser/EnterpriseMyLoans', null, 'Enterprise/myloans'),
       route('EnterpriseUser/EnterpriseLoanDetail', null, 'Enterprise/loanDetail'),
       route('EnterpriseUser/EnterpriseSendMessage', null, 'Enterprise/sendmessage'),

    ]),
    route('login', null, '*'),
  ],
})

/// 导航守卫
const whiteList = ['/login/']
router.beforeEach((to, from, next) => {
 if (localStorage.getItem('UserType')) {
   const userType = localStorage.getItem('UserType')
  //  console.log(userType)
  //  console.log(authenRouter(parseInt(userType), to.path))// 判断完了之后进行相应的处理.
  } else {
    console.log(to.path)
    if (whiteList.includes(to.path)) {
      next()
    } else {
      next('/login/')
    }
  }
  return to.path.endsWith('/') ? next() : next(trailingSlash(to.path))
})

// 导航护卫

export default router
