package com.bughub.lendingplatform.service;

import com.bughub.lendingplatform.entity.FinancialTrade;

import java.util.List;

public interface FinancialTradeService {
    boolean addNewTrade(String borrowerName, String lenderName, Integer productId, Float amount, String bankCard);

    List<FinancialTrade> getTradeByLenderName(String lenderName);

    Object updateIsValid(int tradeInfoId, Boolean isValid);
}
