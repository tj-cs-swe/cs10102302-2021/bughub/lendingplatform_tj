package com.bughub.lendingplatform.mapper;

import com.bughub.lendingplatform.entity.LoanTrade;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Date;
import java.util.List;

@Mapper
@Component("LoanTradeMapper")
public interface LoanTradeMapper {
    int deleteByPrimaryKey(Integer tradeInfoId);

    int insert(LoanTrade record);

    int insertSelective(LoanTrade record);

    LoanTrade selectByPrimaryKey(Integer tradeInfoId);

    int updateByPrimaryKeySelective(LoanTrade record);

    int updateByPrimaryKey(LoanTrade record);

    List<LoanTrade> selectByLenderName(String userName);

    int updateIsValidByPrimaryKey(Integer tradeInfoId, Integer isValid);

    int updateDateValidTime(@RequestParam("tradeInfoId")Integer tradeInfoId, @RequestParam("tradeValidTime")Date tradeValidTime);

    List<LoanTrade> selectByBorrowerName(String userName);
}