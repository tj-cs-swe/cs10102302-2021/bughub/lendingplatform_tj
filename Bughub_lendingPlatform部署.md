# Bughub lendingPlatform部署

## 1.运行环境

1. 在windows环境进行代码编译

2. 在centos（7或8进行部署）

3. 具体软件：

   1. 后端idea编译，java运行

   2. 前端webpack编译，nginx运行

   3. 数据库mariadb

## 2.软件版本

1. idea：IntelliJ IDEA 2021.1.2 (Ultimate Edition)
2. webpack: 4.43.0

3. nginx 1.14.1
4. java：16.0.1

## 3.安装过程

### 1.在服务器安装mariadb

```
yum install mariadb
```

使用方法与指令与mysql完全一致

设置好 用户密码

创建一张表并使用source指令导入sql文件（假设sql文件路径在home下）

```
source /home/lending_platform-v2.sql
```

### 2.在服务器安装nginx

```
yum install nginx
```

找到nginx配置文件目录（默认是这个目录）

```
/etc/nginx/conf.d
```

将lengding.conf文件导入这个文件夹

启动nginx

```
systemctl start nginx
```

### 3.利用idea编译后端代码

使用idea 文件>新建>来自版本控制的项目

![image-20210613221032414](C:\Users\HSY73\AppData\Roaming\Typora\typora-user-images\image-20210613221032414.png)

输入URL进行克隆

![image-20210613230430451](C:\Users\HSY73\AppData\Roaming\Typora\typora-user-images\image-20210613230430451.png)



首先安装openjdk 版本为16.0.1

在项目结构中选择openjdk 16

![image-20210613231539087](C:\Users\HSY73\AppData\Roaming\Typora\typora-user-images\image-20210613231539087.png)



在backend中的pom.xml文件右键，选择添加为Maven，等待Maven配置环境，然后



![image-20210613231927949](C:\Users\HSY73\AppData\Roaming\Typora\typora-user-images\image-20210613231927949.png)



调整application.properties文件中的数据库端口和账号密码



![image-20210613232030317](C:\Users\HSY73\AppData\Roaming\Typora\typora-user-images\image-20210613232030317.png)

右侧Maven菜单，双击clean，再双击install进行编译

在target文件夹中寻找.jar文件上传到服务器目录中

安装java ，如果是centos8可以直接通过yum安装最新版本（16）

如果是centos 7 yum安装只能安装到13，需要通过wget手动安装

在jar文件所在的目录中使用如下指令启动后端

lendingplatform-0.0.1-SNAPSHOT.jar 可换为自己文件的名字

以下指令的意思是通过java启动jar文件，并且通过nohup和最后的&进入后台运行，使程序脱离tty。

```
nohup java -agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=8082 -jar lendingplatform-0.0.1-SNAPSHOT.jar &
```



### 4.利用webpack编译前端代码

在命令行下

npm install 下载相关依赖

npm run serve 开发环境

npm run build 打包生成的生产环境

将编译产生的dist文件夹下的内容上传到/home/frontend中，重启nginx即可启用前端页面





