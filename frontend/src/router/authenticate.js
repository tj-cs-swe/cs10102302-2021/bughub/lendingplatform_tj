// anthenticate权限

import { VSimpleCheckbox } from 'vuetify/lib'

//
const superUserAuthenticate = [
  '/login/',
  '/',
  '/components/profile',
  '/tables/regular',
  '',
]

const companyUserAuthenticate = [
  '/login/',
  '/tables/regular',
  '/',
]

const financeUserAuthenticate = [
  '/login/',
]

export function authenRouter (userType, url) {
  if (userType === 1) {
    return superUserAuthenticate.includes(url)
  }
  if (userType === 2) {
    return companyUserAuthenticate.includes(url)
  }
  if (userType === 3) {
    return financeUserAuthenticate.includes(url)
  }
  return 'wtf'
}
