package com.bughub.lendingplatform.entity;

public class Borrower {
    private String userName;

    private String borrowerName;

    private String borrowerTelephone;

    private String borrowerAddress;

    private String borrowerEmail;

    private String borrowerRankLevel;

    private Float borrowerCreditLevel;

    private Integer borrowCreditPoint;

    private Integer borrowerRiskLevel;

    private String borrowerLink;

    public Borrower(String userName, String borrowerName, String borrowerTelephone, String borrowerAddress, String borrowerEmail, String borrowerRankLevel, Float borrowerCreditLevel, Integer borrowCreditPoint, Integer borrowerRiskLevel, String borrowerLink) {
        this.userName = userName;
        this.borrowerName = borrowerName;
        this.borrowerTelephone = borrowerTelephone;
        this.borrowerAddress = borrowerAddress;
        this.borrowerEmail = borrowerEmail;
        this.borrowerRankLevel = borrowerRankLevel;
        this.borrowerCreditLevel = borrowerCreditLevel;
        this.borrowCreditPoint = borrowCreditPoint;
        this.borrowerRiskLevel = borrowerRiskLevel;
        this.borrowerLink = borrowerLink;
    }

    public Borrower() {
        super();
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getBorrowerName() {
        return borrowerName;
    }

    public void setBorrowerName(String borrowerName) {
        this.borrowerName = borrowerName;
    }

    public String getBorrowerTelephone() {
        return borrowerTelephone;
    }

    public void setBorrowerTelephone(String borrowerTelephone) {
        this.borrowerTelephone = borrowerTelephone;
    }

    public String getBorrowerAddress() {
        return borrowerAddress;
    }

    public void setBorrowerAddress(String borrowerAddress) {
        this.borrowerAddress = borrowerAddress;
    }

    public String getBorrowerEmail() {
        return borrowerEmail;
    }

    public void setBorrowerEmail(String borrowerEmail) {
        this.borrowerEmail = borrowerEmail;
    }

    public String getBorrowerRankLevel() {
        return borrowerRankLevel;
    }

    public void setBorrowerRankLevel(String borrowerRankLevel) {
        this.borrowerRankLevel = borrowerRankLevel;
    }

    public Float getBorrowerCreditLevel() {
        return borrowerCreditLevel;
    }

    public void setBorrowerCreditLevel(Float borrowerCreditLevel) {
        this.borrowerCreditLevel = borrowerCreditLevel;
    }

    public Integer getBorrowCreditPoint() {
        return borrowCreditPoint;
    }

    public void setBorrowCreditPoint(Integer borrowCreditPoint) {
        this.borrowCreditPoint = borrowCreditPoint;
    }

    public Integer getBorrowerRiskLevel() {
        return borrowerRiskLevel;
    }

    public void setBorrowerRiskLevel(Integer borrowerRiskLevel) {
        this.borrowerRiskLevel = borrowerRiskLevel;
    }

    public String getBorrowerLink() {
        return borrowerLink;
    }

    public void setBorrowerLink(String borrowerLink) {
        this.borrowerLink = borrowerLink;
    }
}