package com.bughub.lendingplatform.mapper;

import com.bughub.lendingplatform.entity.Lender;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

@Mapper
@Component("LenderMapper")
public interface LenderMapper {
    int deleteByPrimaryKey(String userName);

    int insert(Lender record);

    int insertSelective(Lender record);

    Lender selectByPrimaryKey(String userName);

    int updateByPrimaryKeySelective(Lender record);

    int updateByPrimaryKeyWithBLOBs(Lender record);

    int updateByPrimaryKey(Lender record);
}