package com.bughub.lendingplatform.mapper;

import com.bughub.lendingplatform.entity.User;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestParam;


@Mapper
@Component("userMapper")
public interface UserMapper {
    int deleteByPrimaryKey(String userName);

    int insert(User record);

    int insertSelective(User record);

    User selectByPrimaryKey(String userName);

    int updateByPrimaryKeySelective(User record);

    int updateByPrimaryKey(User record);

    User[] getAllInactiveUser();

    void addUser(@Param("username")String username, @Param("password")String password, @Param("usertype")int usertype, @Param("usercode")String usercode);
    void registerUserLender(@Param("username")String username);
    void registerUserBorrower(@Param("username")String username);
    void updateEnterpriseInfo(
            @RequestParam("userName")String username,
            @RequestParam("userCode")String usercode,
            @RequestParam("borrowerName")String borrowerName,
            @RequestParam("borrower_telephone")String borrower_telephone,
            @RequestParam("borrower_address")String borrower_address,
            @RequestParam("borrower_email")String borrower_email,
            @RequestParam("borrower_link")String borrower_link
    );
    void updateLenderInfo(
            @RequestParam("userName")String username,
            @RequestParam("userCode")String usercode,
            @RequestParam("lenderName")String lenderName,
            @RequestParam("lender_telephone")String lender_telephone,
            @RequestParam("lender_address")String lender_address,
            @RequestParam("lender_email")String lender_email,
            @RequestParam("lender_link")String lender_link
    );
    int getUserType(@Param("username") String username);
    boolean checkUserExists(@Param("username") String username);
    boolean checkLenderRegistered(@Param("username") String username);
    boolean checkBorrowerRegistered(@Param("username") String username);
    User getUser(@Param("username")String username, @Param("password")String password);
}