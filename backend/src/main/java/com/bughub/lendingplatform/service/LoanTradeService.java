package com.bughub.lendingplatform.service;

import com.bughub.lendingplatform.entity.LoanTrade;

import java.util.List;

public interface LoanTradeService {
    Boolean addNewLoanTrade(String borrowerName, String lenderName, Integer loanId, Float amount, String bankCard);

    List<LoanTrade> showLoanPurchase(String userName);

    Object updateIsValid(int tradeInfoId, Boolean isValid);
}
