package com.bughub.lendingplatform.entity;

import java.util.Date;

public class Message {
    private Integer messageId;

    private String messageSender;

    private String messageReceiver;

    private String messageTitle;

    private Date messageSendTime;

    private Boolean messageIsRead;

    private String messageText;

    public Message(String messageSender, String messageReceiver, String messageTitle, Date messageSendTime, Boolean messageIsRead, String messageText) {
        this.messageSender = messageSender;
        this.messageReceiver = messageReceiver;
        this.messageTitle = messageTitle;
        this.messageSendTime = messageSendTime;
        this.messageIsRead = messageIsRead;
        this.messageText = messageText;
    }

    public Message(Integer messageId, String messageSender, String messageReceiver, String messageTitle, Date messageSendTime, Boolean messageIsRead, String messageText) {
        this.messageId = messageId;
        this.messageSender = messageSender;
        this.messageReceiver = messageReceiver;
        this.messageTitle = messageTitle;
        this.messageSendTime = messageSendTime;
        this.messageIsRead = messageIsRead;
        this.messageText = messageText;
    }

    public Message() {
        super();
    }

    public Integer getMessageId() {
        return messageId;
    }

    public void setMessageId(Integer messageId) {
        this.messageId = messageId;
    }

    public String getMessageSender() {
        return messageSender;
    }

    public void setMessageSender(String messageSender) {
        this.messageSender = messageSender;
    }

    public String getMessageReceiver() {
        return messageReceiver;
    }

    public void setMessageReceiver(String messageReceiver) {
        this.messageReceiver = messageReceiver;
    }

    public Date getMessageSendTime() {
        return messageSendTime;
    }

    public void setMessageSendTime(Date messageSendTime) {
        this.messageSendTime = messageSendTime;
    }

    public Boolean getMessageIsRead() {
        return messageIsRead;
    }

    public void setMessageIsRead(Boolean messageIsRead) {
        this.messageIsRead = messageIsRead;
    }

    public String getMessageText() {
        return messageText;
    }

    public void setMessageText(String messageText) {
        this.messageText = messageText;
    }

    public String getMessageTitle() {
        return messageTitle;
    }

    public void setMessageTitle(String messageTitle) {
        this.messageTitle = messageTitle;
    }
}