package com.bughub.lendingplatform;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LendingplatformApplication {

    public static void main(String[] args) {
        SpringApplication.run(LendingplatformApplication.class, args);
    }

}
