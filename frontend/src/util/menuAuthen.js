const superUser = [
  {
    title: 'Mainpage',
    icon: 'mdi-view-dashboard',
    to: '/mainpage',
  },
  {
    title: 'SendMessage',
    icon: 'mdi-bell',
    to: '/Enterprise/sendmessage/',
  },
  {
    title: 'JudgeRegister',
    icon: 'mdi-bell',
    to: '/judge/register',
  },
  {
    title: 'JudgeApplying',
    icon: 'mdi-bell',
    to: '/judge/Applying',
  },
]

// route('EnterpriseUser/EnterpriseProducts', null, 'Enterprise/products'),
// route('EnterpriseUser/EnterpriseProductDetail', null, 'Enterprise/productDetail'),
// route('EnterpriseUser/EnterpriseLoans', null, 'Enterprise/loans'),
// route('EnterpriseUser/EnterpriseLoanDetail', null, 'Enterprise/loanDetail'),
// route('EnterpriseUser/EnterpriseSendMessage', null, 'Enterprise/sendmessage'),
const companyUser =
  [
    {
      title: 'Mainpage',
      icon: 'mdi-view-dashboard',
      to: '/mainpage',
    },
    {
      title: 'EnterpriseProducts',
      icon: 'mdi-bell',
      to: '/Enterprise/products/',
    },
    {
      title: 'EnterpriseLoans',
      icon: 'mdi-bell',
      to: '/Enterprise/loans/',
    },
    {
      title: 'myLoans',
      icon: 'mdi-bell',
      to: '/Enterprise/myloans/',
    },
    {
      title: 'myProducts',
      icon: 'mdi-bell',
      to: '/Enterprise/myproducts/',
    },
    {
      title: 'EnterpriseSendMessage',
      icon: 'mdi-bell',
      to: '/Enterprise/sendmessage/',
    },
    {
      title: 'Myinformation',
      icon: 'mdi-bell',
      to: '/enterpriseinformation',
    },

  ]

const financeUser =
[
  {
    title: 'Mainpage',
    icon: 'mdi-view-dashboard',
    to: '/mainpage',
  },
  {
    title: 'PublishMessage',
    icon: 'mdi-bell',
    to: '/Enterprise/sendmessage/',
  },
  {
    title: 'PublishLoan',
    icon: 'mdi-bell',
    to: '/financial/sendloan/',
  },
  {
    title: 'PublishProduct',
    icon: 'mdi-bell',
    to: '/financial/sendProduct/',
  },
  {
    title: 'myLoans',
    icon: 'mdi-bell',
    to: '/Enterprise/myloans/',
  },
  {
    title: 'myProducts',
    icon: 'mdi-bell',
    to: '/Enterprise/myproducts/',
  },
]

// route('FinancialUser/FinancialProducts', null, 'financial/products'),
// route('FinancialUser/FinancialProductDetail', null, 'financial/productDetail'),
// route('FinancialUser/FinancialLoans', null, 'financial/loans'),
// route('FinancialUser/FinancialLoanDetail', null, 'financial/loanDetail'),
// route('FinancialUser/FinancialSendMessage', null, 'financial/sendmessage'),
// route('FinancialUser/FinancialSendLoan', null, 'financial/sendloan'),
// route('FinancialUser/FinancialSendProduct', null, 'financial/sendProduct'),

export default {
  financeUser,
  companyUser,
  superUser,
}
