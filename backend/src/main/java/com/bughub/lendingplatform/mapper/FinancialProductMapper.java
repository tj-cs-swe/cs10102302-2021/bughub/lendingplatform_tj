package com.bughub.lendingplatform.mapper;

import com.bughub.lendingplatform.entity.FinancialProduct;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

import java.util.List;

@Mapper
@Component("FinancialProductMapper")
public interface FinancialProductMapper {
    int deleteByPrimaryKey(Integer productId);

    int insert(FinancialProduct record);

    int insertSelective(FinancialProduct record);

    FinancialProduct selectByPrimaryKey(Integer productId);

    int updateByPrimaryKeySelective(FinancialProduct record);

    int updateByPrimaryKey(FinancialProduct record);

    List<FinancialProduct> selectAllProduct();

    FinancialProduct selectByCodeAndName(String productCode, String productName);

    List<FinancialProduct> selectByProductProvider(String productProvider);


}