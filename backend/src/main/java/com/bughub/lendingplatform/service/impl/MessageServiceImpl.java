package com.bughub.lendingplatform.service.impl;

import com.bughub.lendingplatform.entity.Message;
import com.bughub.lendingplatform.entity.User;
import com.bughub.lendingplatform.mapper.MessageMapper;
import com.bughub.lendingplatform.mapper.UserMapper;
import com.bughub.lendingplatform.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class MessageServiceImpl implements MessageService {
    @Autowired
    MessageMapper messageMapper;

    @Qualifier("userMapper")
    @Autowired
    UserMapper userMapper;

    @Override
    public List<Message> getMessageSent(String messageSender){
        return messageMapper.selectBySender(messageSender);
    }

    @Override
    public List<Message> getMessageReceived(String messageReceiver){
        return messageMapper.selectByReceiver(messageReceiver);
    }

    @Override
    public boolean sendMessage(String messageSender, String messageReceiver, String messageTitle, String messageText){

        User sender = userMapper.selectByPrimaryKey(messageSender);
        if (sender == null)
            return false;

        User receiver = userMapper.selectByPrimaryKey(messageReceiver);
        if (receiver == null)
            return false;

        return messageMapper.insert(new Message(messageSender, messageReceiver, messageTitle, new Date(), false, messageText)) == 1;
    }
}
